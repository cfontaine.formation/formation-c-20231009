#include <stdio.h>
#include <stdlib.h>
#include "exemple.h"
#include "menu.h"

int main(void)
{
    /* Rappel pointeur*/
    int x = 10;
    printf("%d %p\n", x, &x);

    /* Declaration du pointeur*/
    int *ptrX = &x;
    printf("%p\n", ptrX);

    /* Contenu de la variable pointée */
    printf("%d\n", *ptrX);
    *ptrX = 40;
    printf("%d\n", x);

    /* Pointeur constant */
    /* En préfixant avec const un pointeur*/
    const int *ptrConstX = &x;
    printf("%d", *ptrConstX); /* 40 */
    /*ptrConstX=100;*/
    ptrConstX = NULL;

    /* En postfixant avec const un pointeur => pointeur devient constant S*/
    int *const ptrXConst = &x;
    /*ptrXConst=NULL;*/
    printf("%d\n", *ptrXConst); /*40*/
    *ptrXConst = 200;
    printf("%d\n", x); /* 200 */

    const int *const ptrConst = &x;
    printf("%d\n", *ptrConst);
    /**ptrConst=3000;*/
    /*ptrConst=NULL;*/

    /* Affectation de pointeur */
    /* On peut affecter des pointeur du même type ou avec NULL */
    int *ptr1 = &x;
    int *ptr2;
    ptr2 = ptr1;
    printf("ptr1=%p ptr2=%p %d %d\n", ptr1, ptr2, *ptr1, *ptr2);
    ptr1 = NULL;
    printf("ptr1=%p ptr2=%p %d\n", ptr1, ptr2, *ptr2);

    /*Affecter des Pointeurs de type différent -> (cast)*/
    int v = 0x20214041;
    printf("%x\n", v);
    int *ptrV = &v;
    char *ptrC = (char *)ptrV;
    printf("%x\n", *ptrC);

    /* Arthimétque des pointeurs */
    /* Addition,
     - on peut ajouter une valeur entière à un pointeur
     - on peut incrémenter le pointeur */

    printf("%x\n", *(ptrC + 1));
    printf("%x\n", *(ptrC + 2));
    printf("%x\n", *(ptrC + 3));
    ptrC++;
    printf("%x\n", *ptrC);

    /* soustraction */
    char *ptrCFin = ptrC + 2;
    printf("distance netre les pointeurs= %d", ptrCFin - ptrC);

    ptrC--;
    printf("%x\n", *ptrC);

    /* Comparaison */
    double d1 = 4.5;
    double d2 = 5.6;

    double *p1 = &d1;
    double *p2 = &d1;
    double *p3 = &d2;

    if (p1 == p2)
    {
        printf("p1=p2\n");
    }
    if (p1 == p3)
    {
        printf("p1 == p3\n");
    }
    else
    {
        printf("p1 != p3\n");
    }

    p2 = NULL;
    if (p2 == NULL) /*!p2*/
    {
        printf("p2 == NULL\n");
    }

    /* pointeur et tableau */
    int t1[] = {2, 6, 7, 3, 12, 34, 8, 5, 23, 56};

    /* t1 équivaut à un pointeur sur le premier */
    int *ptr = t1;
    printf(" %p %p\n", t1, ptr);
    printf("%d\n", *ptr);                 /*2*/
    printf("%d %d\n", *(ptr + 1), t1[1]); /*6*/
    printf("%d %d\n", *(ptr + 1), ptr[1]);
    printf("%d %d\n", *(t1 + 2), t1[2]);
    ptr++;
    printf("%d\n", ptr[-1]);

    /* tableau à 2 dimmensions */
    int tab2d[3][2] = {{3, 5}, {23, 15}, {31, 6}};
    printf("%d %d\n", tab2d[0][1], tab2d[1][0]);
    printf("%d %d\n", *(tab2d + 1), *(tab2d + 2)); /*5 23*/

    /* Passage en paramètre de tableau */
    testParamTab(t1, 10);
    testParamTabPtr(t1, 10);

    /* Pointeur généraliste void* */
    double d = 123.456;
    double *ptrD = &d;
    void *ptrGen = ptrD;
    printf("%f\n", *ptrD);
    /*printf("%d\n",*ptrGen);*/
    /*ptrGen++;*/
    double *ptrD2 = ptrGen;
    printf("\"\\%f\"\n", *ptrD2);

    /* Allocation Dynamique */
    int *ptrA = malloc(sizeof(int)); /* 4 octets*/
    if (ptrA != NULL)
    {
        *ptrA = 123;
        printf("%d\n", *ptrA);
        printf("%p\n", ptrA);
        free(ptrA);
        printf("%p\n", ptrA);
        ptrA = NULL;
    }

    int st = 5;
    printf("Saisir la taille du tableau=");
    scanf("%d", &st);
    int *tabDyn = malloc(st * sizeof(int));
    if (tabDyn != NULL)
    {
        int i;
        for (i = 0; i < st; i++)
        {
            tabDyn[i] = 0;
        }
        tabDyn[0] = 6;
        testParamTab(tabDyn, st);
        free(tabDyn);
        tabDyn = NULL;
    }

    tabDyn = calloc(st, sizeof(int));
    if (tabDyn != NULL)
    {
        tabDyn[0] = 9;
        testParamTab(tabDyn, st);
        free(tabDyn);
        tabDyn = NULL;
    }

    /*realloc*/
    char *ptrChr = malloc(100);
    if (ptrChr != NULL)
    {
        ptrChr = realloc(ptrChr, 15);
        int i;
        for (i = 0; i < 15; i++)
        {
            ptrChr[i] = 'a' + i;
        }
        for (i = 0; i < 15; i++)
        {
            printf("%c", ptrChr[i]);
        }
        free(ptrChr);
        ptrChr = NULL;
    }

    int *t2 = testRetourTableau(6, 10);
    if (t2 != NULL)
    {
        testParamTab(t2, 6);
        free(t2);
        t2 = NULL;
    }

    int *t3 = NULL;
    printf("%p\n", t3);
    testParamAdresse(&t3, 5, 1);
    printf("%p\n", t3);

    int *t4 = testRetourLocal();
    printf("%d", t4[3]);
    free(t4);

    /* Pointeur de fonction */
    double (*ptrFonction)(double, double);
    int choix;
    scanf("%d", &choix);
    if (choix == 1)
    {
        ptrFonction = addition;
    }
    else
    {
        ptrFonction = multiplication;
    }
    int i;
    for (i = 0; i < 5; i++)
    {
        printf("%f\n", ptrFonction(i, 4.5));
    }

    printf("%f\n", calcul(3.4, 5.6, addition));
    printf("%f\n", calcul(3.4, 5.6, multiplication));

    menu();
    return 0;
}