#include "tableau.h"
#include <stdio.h>
#include <stdlib.h>

int *saisirTab(int *size)
{
    int s;
    int *tab = NULL;
    printf("Nombre d element du tableau ");
    scanf("%d", &s);
    if (s > 0)
    {
        *size = s;
        tab = malloc(s * sizeof(int));
        if (tab != NULL)
        {
            int i;
            for (i = 0; i < s; i++)
            {
                printf("t[%d]=",i);
                scanf("%d", &tab[i]);
            }
            return tab;
        }
    }
    return NULL;
}

void afficherTab(int tab[], int size)
{
    printf("[ ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("]\n");
}

int *redimTab(int tab[], int oldSize, int newSize)
{
    int *t = realloc(tab, newSize);
    int i;
    for (i = oldSize; i < newSize; i++)
    {
        t[i] = 0;
    }
    return t;
}

void calculTab(int tab[], int size, double *moyenne, int *maximum)
{
    *maximum = tab[0];
    double somme = 0.0;
    int i;
    for (i = 0; i < size; i++)
    {
        if (tab[i] > *maximum)
        {
            *maximum = tab[i];
        }
        somme += tab[i];
    }
    *moyenne = somme / size;
}