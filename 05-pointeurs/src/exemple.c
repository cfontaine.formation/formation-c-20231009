#include "exemple.h"
#include <stdio.h>
#include <stdlib.h>

void testParamTab(int t[], int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        printf("t[%d]=%d", i, t[i]);
    }
}

void testParamTabPtr(int *t, int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        printf("t[%d]=%d", i, t[i]);
    }
}

int *testRetourTableau(int size, int initVal)
{
    int *ptr = malloc(size * sizeof(int));
    if (ptr != NULL)
    {
        int i;
        for (i = 0; i < size; i++)
        {
            ptr[i] = initVal;
        }
    }
    return ptr;
}

void testParamAdresse(int **tab, int size, int initVal)
{
    *tab = malloc(size * sizeof(int));
    if (*tab != NULL)
    {
        int i;
        for (i = 0; i < size; i++)
        {
            (*tab)[i] = initVal;
        }
    }
}

int* testRetourLocal(void)
{
    /*int t[5]={0};*/
    int* t=malloc(5*sizeof(int));
    /* */
    return t;
}


double addition(double a,double b)
{
    return a+b;
}

double multiplication(double a,double b)
{
    return a*b;
}

double calcul(double a , double b, double(*f)(double,double))
{
    return f(a,b);
}