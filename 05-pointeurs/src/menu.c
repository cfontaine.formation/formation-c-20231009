#include <stdio.h>
#include <stdlib.h>
#include "menu.h"         
#include "tableau.h"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             

void afficherMenu()
{
    printf("\n1 - saisir un tableau\n");
    printf("2 - afficher un tableau\n");
    printf("3 - redimensionner le tableau\n");
    printf("4 - afficher le maximum et la moyenne du tableau\n");
    printf("0 - quitter\n");
    printf("choix=");
}

int saisirMenu()
{ 
    int choix;
    for(;;)
    {
        fflush(stdin);
        scanf("%d",&choix);
        if(choix<0 | choix>4)
        {
            printf("Choix: %d non valide\n",choix);
        }
        else
        {
            printf("\n");
            return choix;
        }
    }

}

void menu(void)
{
    int* t=NULL;
    int size=0;
    int choix=0;

    do{
        afficherMenu();
        choix=saisirMenu();
        switch(choix)
        {
            case 1:
                if(t!=NULL)
                {
                    free(t);
                }
                t=saisirTab(&size);
            break;
            case 2:
                if(t!=NULL)
                {
                    afficherTab(t,size);
                }
            break;
            case 3:
                if(t!=NULL)
                {
                    int newSize;
                    printf("saisir le nouvelle taille du tableau (%d)",size);
                    scanf("%d",&newSize);
                    if(newSize>0)
                    {
                        t=redimTab(t,size,newSize);
                        size=newSize;
                    }
                }
            break;
            case 4:
                if(t!=NULL)
                {
                    double moyenne;
                    int maximum;
                    calculTab(t,size,&moyenne,&maximum);
                    printf("moyenne=%f maximum=%d\n",moyenne,maximum);
                }
        }
    }while(choix!=0);
    if(t!=NULL) 
    {
        free(t);
    }

}