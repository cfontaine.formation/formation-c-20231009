
void testParamTab(int[], int);

void testParamTabPtr(int*, int);

int* testRetourTableau(int,int );

void testParamAdresse(int**,int,int );

int* testRetourLocal(void);

double addition(double,double);

double multiplication(double,double);

double calcul(double , double, double(*f)(double,double));
