#include <stdio.h> /* inclure le fichier d'en-tête stdio.h qui contient les entrée/sortie standard */

#define NOMBRE_HEURE 7 /* définir une constante en utilisant la directive de préprocesseur #define */

/* Fonction main => point d'entré du programme */
int main(void)
{

   /* Déclaration d'une variable*/
   int i;

   /* Intialisation de la variable*/
   i = 10;

   /* Déclaration multiple de variable*/
   double md, me;

   /* Déclaration et intialisation d'une variable*/
   double hauteur = 12.3;

   /* Déclaration et intialisation multiple de variable */
   double mdd = 0, mee = 2;

   /* Littéral -> valeur constant placé directement dans le programme */

   /* Littéral entier -> par défaut de type int */
   long l = 1000L;         /* Littéral long -> L */
   unsigned int ui = 345U; /* Littéral long -> U */

   /* Littéral entier -> changement de base*/
   int dec = 42;   /* base par défaut: décimal base 10 */
   int hex = 0xF4; /* base 16 -> 0x */
   int oct = 043;  /* base 8 -> 0 */
   printf("décimal=%d hexadecimal=%x octal=%o\n", dec, hex, oct);

   /* Littéral virgule flottante */
   double longueur = 456.6;
   double largeur = 4.56e3; /* écriture exponentiel */
   printf("Longueur=%f, Largeur est egale a %f\n", longueur, largeur);

   /* Littéral virgule flottante => par défaut de type double */
   float f = 3.45F; /* Litgéral float -> F*/

   /* Littéral caractère */
   char chr = 'b';        /* Littéral caractère entre simple quote '' */
   char chrOctal = '\51'; /* Littéral caractère en octal -> '\ ' */
   char chrHex = '\x4d';  /* Littéral caractère en hexadécimal -> '\x ' */
   printf(" %c %c %c", chr, chrOctal, chrHex);

   /* printf -> affichage dans la console
      format -> "i=%d le caractere=%c\n"
      %d -> correspont une variable entière
      %c -> correspont une variable caractere
      \n -> au retour à la ligne */
   printf("i=%d le caractere=%c\n", i, chr);

   /* Afficher un seul caractère */
   putchar(chr);

   /* Saisir un seul caractère */
   chr = getchar();
   printf("le caractere saisie:%c", chr);

   /* scanf -> saisie sur l'entrée standard (clavier)
      format "%d" -> on saisie un entier
      la valeur saisie sera placé dans la variable i */
   scanf("%d", &i);
   printf("i=%d", i);

   /* format "%lf %c"
     % lf -> on saisie un double   la valeur saisie sera placé dans la variable largeur
     %c ->  on saisie un caractère la valeur saisie sera placé dans la variable chr  */
   scanf("%lf %c", &largeur, &chr);
   printf("largeur=%f chr=%c", largeur, chr);

   /* Constante */

   /* Constante en utilisant #define */
   int nbHeure = NOMBRE_HEURE;
   printf("%d\n", NOMBRE_HEURE);

   /* Constante en utilisant const */
   const int CST = 123; /* On est obligé d'initialiser une constante à l'initialisation*/
   printf("%d\n", CST);
   /*CST=1;  une fois la constante définit, on ne peut plus changer sa valeur */

   /* Convertion implicite => rang inférieur vers un rang supérieur (pas de perte de donnée)
   convertion d'un entier vers un double*/
   int ci1 = 34;
   double ci2;
   ci2 = ci1;
   printf("%d %f", ci1, ci2);

   /* Convertion d'un carctère vers un entier */
   char ci3 = 'a';
   int ci4 = ci3; /* Dans la variable entière va être stocker la valeur numérique 97 (code ASCII) qui correspond au caractère a */
   printf("%c %d %d\n", ci3, ci3, ci4);
   /* Dans le format "%c %d %d\n"
      pour afficher le carctère contenu dans le  char, on utilise %c
      pour afficher la valeur numérique d'un char, on utilise %d */

   /* Convertion explicite => opérateur de cast (type) */
   int ce1 = 11;
   double cres1 = ce1 / 2;           /* 5*/
   double cres2 = ((double)ce1) / 2; /* 5.5*/
   printf("%f %f\n", cres1, cres2);

   /* Opérateur d'affectation fait toujours la conversion (implicite et explicite)*/
   ce1 = cres2; /* 5*/
   printf("%f %d\n", cres2, ce1);

   /* Dépassement de capacité*/
   int dep = 300;         /* 0001 0010 1100	300 => 300 > 127, la valeur maximal d'un char*/
   signed char cep = dep; /*      0010 1100	44 => dans cep, on n'a que le premier octet de dep */
   printf("%d %d\n", dep, cep);

   /* Opérateur */
   /*Opérateur arithméthique */
   int op1 = 12;
   int op2 = 30;
   int res1 = op1 + op2;
   printf("%d %d \n", res1, 3 % 2);

   /* Opérateur d'incrémentation */
   /* pré-incrémentation */
   int inc = 0;
   int res2 = ++inc; /* équivalanr à inc =1 res2= 1*/
   printf("inc=%d res2=%d ", inc, res2);

   /* post-incrémentation*/
   inc = 0;
   res2 = inc++; /* res=0 inc=1*/
   printf("inc=%d res2=%d", inc, res2);

   /* Affectation composée*/
   inc = 5;
   inc += 10;               /* inc=inc+10 */
   printf("inc=%d\n", inc); /*inc=15*/

   /* Atelier Somme
    Saisir 2 nombres décimaux et afficher le résultat dans la console sous la forme 1.5 __+__ 3.6 __=__ 5.1
    */
   int v1, v2;
   printf("Saisir 2 entiers ");
   scanf("%d %d", &v1, &v2);
   int addition = v1 + v2;
   printf("%d + %d = %d\n", v1, v2, addition);

   /* Opérateur de comparaison*/
   inc = 15;
   printf("%d\n", inc == 5); /* faux -> résultat de la comparaison égale à 0*/

   printf("%d\n", inc > 5); /* vrai ->  résultat de la comparaison différente de 0*/

   /* Opéretur non ! */
   printf("%d\n", !(inc == 5)); /*vrai >0*/

   /* Opérateur et && et ou  || */
   /*                ET       OU
      a      b     a && b   a || b
      Faux  Faux    Faux     Faux
      Vrai  Faux    Faux     Vrai
      Faux  Vrai    Faux     Vrai
      Vrai  Vrai    Vrai     Vrai

      Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
      && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
      || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
   */

   printf(" ET=%d \n", inc < 12 && ++inc < 100); /* faux */
   printf("inc=%d", inc);

   printf(" OU=%d \n", inc == 15 || ++inc > 100); /* vrai */
   printf("inc=%d", inc);

   /* Opérateur binaire */
   /* Pour les opérations sur des binaires, on peut utiliser les entiers non signé */
   unsigned int bin = 0x13; /* 13 en hexadecimal -> 00010011*/

   /* Complément ~*/
   printf("%x\n", ~bin); /* 11111111 11111111 11111111 11101100*/

   /* et &*/
   printf("%x\n", bin & 0x2); /*  00010011
                                & 00000010 = 10  0X2*/
   /* ou |*/
   printf("%x\n", bin | 0x4); /*  00010011
                                | 00000100 =  10111   0x17*/

   /* OU exclusif
      a      b     a ^ b
      Faux  Faux   Faux
      Vrai  Faux   Vrai
      Faux  Vrai   Vrai
      Vrai  Vrai   Faux
   */

   /* ou exclusif ^ */
   printf("%x", bin ^ 0x6); /*  00010011
                              ^ 00000110 =  10101   0x15*/

   /* Opérateur de décalage*/
   printf("%x\n", bin >> 1); /* 0001001      0x9*/
   printf("%x\n", bin >> 3); /* 00010        0X2*/
   printf("%x\n", bin << 1); /* 00010 0110   0X26*/
   printf("%x\n", bin << 4); /* 000100110000 0x130*/

   /* Opérateur sizeof: Renvoie la taille en octet d’une variable ou d’un type  */
   printf("%d %d", sizeof(inc), sizeof(double));

   /*Ajustement de type*/
   int pn1 = 11;
   double pn2 = 4.5;         /* pn1 va être convertie en double avant de faire l'addition */
   double pres1 = pn1 + pn2; /* 15.5 */

   double pres2 = pn1 / 2;           /*5.0*/
   double pres3 = ((double)pn1) / 2; /*5.5*/
   double pres4 = pn1 / 2.0;         /*5.5*/

   printf("%f %f %f %f", pres1, pres2, pres3, pres4);

   /* Promotion numérique char et short -> int */
   short s1 = 2;
   short s2 = 4;
   int pres5 = s1 + s2; /* s1 et s2 vont être converti vers un entier avant d'être ajouté */

   /* Pointeur */
   double valeur = 12.5;

   /* Déclaration d'un pointeur de double et initialisation à NULL */
   double *ptr = NULL; /* NULL -> Un pointeur qui ne pointe sur rien */

   /* &valeur => adresse de la variable valeur */
   printf("valeur=%f Adresse de la variable valeur%p\n", valeur, &valeur);

   ptr = &valeur;
   /* on aurait pu déclarer ptr aussi avec: double* ptr=&valeur;*/
   printf("contenu du pointeur ptr=%p\n", ptr);

   /* *ptr => Accès au contenu de la variable pointée par ptr */
   printf("contenu de ce qui est pointé par le pointeur ptr=%f\n", *ptr);
   *ptr = 56.7; /*  modification de la variable x par l'intermédiaire du pointeur ptr */
   printf("%f\n", valeur);
   
   return 0;
}