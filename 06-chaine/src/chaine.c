#include "chaine.h"
#include <stdlib.h>
#include <string.h>

char *inverser(char *str)
{
    int size = strlen(str);
    char *s = malloc(size + 1);
    if(s!=NULL)
    {
        int i;
        for (i = 0; i < size; i++)
        {
            s[i] = str[size -1 - i];
        }
        s[size] = '\0';
    }
    return s;
}

int palindrome(char *str)
{
    char *inv=inverser(str);
    int r=0;
    if(inv!=NULL)
    {
        r=strcmp(str,inv)==0;
        free(inv);
    }
    return r;
}