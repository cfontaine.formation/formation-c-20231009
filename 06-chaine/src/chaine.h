#ifndef CHAINE_H
#define CHAINE_H

#define ADDITION(a,b) (a+b)

char* inverser(char *str);

int palindrome(char *str);

#if __WIN32__
    #define EXEMPLE "windows"
#else
   #define EXEMPLE "autre"
#endif 

#endif 