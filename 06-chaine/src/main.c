#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include "chaine.h"

int main(void)
{
    /* Chaine constante */
    char* str1="Hello World";
    printf("%s\n",str1);
    /*str1[0]='h'*/;

    /* Chaine de caractères */
    char str[22]="hello world"; /*hello world\0*/
    str[0]='H';
    printf("%s (%d)\n",str,sizeof(str));

    char vide[]=""; /*\0*/
    printf("%d\n",sizeof(vide));

    /*Longeur d'une chaine -> strlen   */
    printf("%d\n", strlen(str)); /*11 caractètres*/

    /* concaténation => strcat */
    strcat(str,"!!!!");
    printf("1-> %s\n",str);

    strncat(str,"--------",2);
    printf("2-> %s\n",str);

    /* Comparaison de chaine => strcmp */
    char str2[]="hello__world";
    char str3[]="hello world";
    printf("3->%d\n",strcmp(str2,str3)); /*différent de 0*/ 
    printf("4->%d\n",strncmp(str2,str3,5)); /* == 0*/

    /* Copier une chaine => strcpy */
    char str4[20], str5[20];
    strcpy(str4,str3);
    printf("%s\n",str4);
    strncpy(str5,str3,5);
    str5[5]='\0';
    printf("%s\n",str5);

    /* Rechercher un caractère => strchr ou une chaine => strstr */
    char* pos1= strchr(str3,'o');
    printf("indice de o=%d\n",pos1-str3 ); /*4*/

    char* pos2= strrchr(str3,'o');
    printf("indice de o=%d\n",pos2-str3 ); /*7*/

    char* posStr= strstr(str3,"rl");
    printf("indice de rl=%d\n",posStr-str3 ); /*8*/
    posStr= strstr(str3,"aaaa");
    printf("%p\n",posStr);

    char* inv=inverser("hello world");
    printf("%s\n",inv);
    free(inv);

    printf("radar -> %d\n",palindrome("radar"));
    printf("bonjour -> %d\n",palindrome("bonjour"));
    printf("sos -> %d\n",palindrome("sos"));

    /* convertion chaine -> nombre */
    /* vers un int */
    printf("%d\n",atoi("456")); 
    printf("%d\n",atoi("45aze"));
    printf("%d\n",atoi("bonjour"));

    /* vers un long */
    printf("%d\n",atol("456"));

    /* vers un double */
    printf("%f\n",atof("12.34"));

    printf("%d\n",isalnum('2'));
    printf("%d\n",isalnum('a'));
    printf("%d\n",isalnum('_'));

    printf("%d\n",isalpha('2'));
    printf("%d\n",isalpha('y'));

    printf("%d\n",isupper('A'));
    printf("%d\n",isupper('a'));

    printf("%c\n",tolower('A'));
    printf("%c\n",toupper('a'));
 
    printf("%f\n",pow(3.0,2.0));
    printf("%f\n",sqrt(4.0));
    printf("%f\n",cos(M_PI/2));
    printf("%f\n",sin(M_PI/2));
    printf("%f\n",trunc(1.4));
    printf("%f\n",round(2.4));
    printf("%f\n",round(2.6));
    printf("%f\n",ceil(2.2));
    printf("%f\n",floor(2.6));
    printf("%d\n",rand());

    /* Exemple macro*/
    printf("macro %d\n",ADDITION(10,32));

    printf("%s",EXEMPLE);
    return 0;
}