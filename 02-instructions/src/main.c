#include <stdio.h>

int main(void)
{
    /* Condition if*/
    int val;
    scanf("%d", &val);

    if (val > 10)
    {
        printf("val est superieur à 10\n");
    }
    else if (val == 10)
    {
        printf("val est egale a 10\n");
    }
    else
    {
        printf("val est inferieur a 10\n");
    }

    /* Atelier Moyenne
    Saisir 2 nombres entiers et afficher la moyenne dans la console */
    int a, b;
    scanf("%d %d", &a, &b);
    double moyenne = (a + b) / 2.0;
    printf("moyenne=%f", moyenne);

    /* Atelier Trie de 2 Valeurs
    Saisir 2 nombres à virgule flottante et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5 */
    double va, vb;
    scanf("%lf %lf", &va, &vb);
    if (va < vb)
    {
        printf("%f < %f", va, vb);
    }
    else
    {
        printf("%f < %f", vb, va);
    }
    /* Atelier Intervalle
       Saisir un nombre entier et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus) */
    int vi;
    scanf("%d", &vi);
    if (vi > -4 && vi <= 7)
    {
        printf("%d fait partie de l'interval", vi);
    }

    /* Condition switch */
    int numJour;
    scanf("%d", &numJour);
    switch (numJour)
    {
    case 1:
        printf("Lundi");
        break;
    case 6:
    case 7:
        printf("Week-end");
        break;
    default:
        printf("un autre jour");
    }

    /* Atelier Calculatrice
        Faire un programme calculatrice
        Saisir dans la console

        un double
        une caractère opérateur qui a pour valeur valide : + - * /

        un double

        Afficher:

        Le résultat de l’opération
        Une message d’erreur si l’opérateur est incorrecte
        Une message d’erreur si l’on fait une division par 0
    */

    double v1, v2;
    char op;
    scanf("%lf %c %lf", &v1, &op, &v2);
    switch (op)
    {
    case '+':
        printf("= %f\n", v1 + v2);
        break;
    case '-':
        printf("= %f\n", v1 - v2);
        break;
    case '*':
        printf("= %f\n", v1 * v2);
        break;
    case '/':
        if (v2 == 0)
        {
            printf("Division par 0\n");
        }
        else
        {
            printf("= %f\n", v1 / v2);
        }
        break;
    default:
        printf("L operateur: %c est incorrecte\n", op);
    }

    /* Opérateur ternaire */
    double val;
    scanf("%lf", &val);
    double abs = val < 0 ? -val : val;
    printf("|%f|=%f", val, abs);

    /* Boucle: while */
    int i = 0;
    while (i < 5)
    {
        printf("i=%d\n", i);
        i++;
    }

    /* Boucle: do while */
    i = 0;
    do
    {
        printf("i=%d\n", i);
        i++;
    } while (i < 5);

    /* Boucle: for */
    for (i = 0; i < 5; i++)
    {
        printf("i=%d\n", i);
    }

    for (i = 10; i > 0; i -= 2)
    {
        printf("i=%d\n", i);
    }

    /* Instructions de branchement */
    /* break */
    for (i = 0; i < 10; i++)
    {
        if (i == 3)
        {
            break; /* break -> on quitte la boucle */
        }
        printf("i=%d\n", i);
    }

    /* continue */
    for (i = 0; i < 10; i++)
    {
        if (i == 3)
        {
            continue; /* continue -> on passe à l'ittération suivante */
        }
        printf("i=%d\n", i);
    }

    /* goto */
    int j = 0;
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 5; j++)
        {
            if (i == 3)
            {
                goto EXIT_LOOP; /* unique utilisation de goto -> sortir de boucle imbriquées */
            }
            printf("i=%d j=%d\n", i, j);
        }
    }
EXIT_LOOP:
    /*
     Atelier: Table de multiplication
     Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
     1 X 4 = 4
     2 X 4 = 8
     …
     9 x 4 = 36
     Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
    */

    int i;
    int m;
    for (;;)
    {
        scanf("%d", &m);
        if (m < 1 || m > 9)
        {
            break;
        }
        for (i = 1; i < 10; i++)
        {
            printf("%dx%d=%d\n", i, m, i * m);
        }
    }

    /*
    Atelier Quadrillage
    Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
    */
    int col, row, c, r;
    scanf("%d %d", &col, &row);
    for (r = 0; r < row; r++)
    {
        for (c = 0; c < col; c++)
        {
            printf("[ ]");
        }
        printf("\n");
    }
    
    return 0;
}