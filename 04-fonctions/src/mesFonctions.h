
/* Fichier .h -> contient la déclaration des fonctions */

/* Déclaration d'une fonction */
double multiplier(double, double);

/* Déclaration d'une fonction sans retour -> void */
void afficher(int);

/* Atelier : fonction maximum */
int maximum(int, int);

/* Atelier : fonction paire */
int even(int);

/* Passage de paramètre par valeur */
void testParamValeur(int);

/* Passage de paramètre par adresse
   En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre */
void testParamAdresse(int *);
void incrementer(int *);

/* Atelier : permuter */
void permuter(int *, int *);

/* Passage d'un tableau, comme paramètre d'une fonction
  On ne peut pas passer un tableau par valeur uniquement par adresse */
void afficherTab(int[], int);

/* Récursivité */
int factorial(int);

/* Fonction à nombre variable de paramètre */
double moyenne(int, ...);

/* Atelier: bissextile */
int bissextile(int);

/* Atelier: dernier jour du mois */
int dernierJourMois(int, int);