#include <stdio.h>
#include "mesFonctions.h"
#include "classeMemorisation.h"

/* Variable globale*/
int varGlobal;

/* avec static la variable globale n'est visible que dans ce fichier */
static int varGlobalStatic;

const double PI = 3.14;

/* Déclaration des fonctions => déplacer dans le fichier .h d'en-tête */
/* int even(int);
int maximum(int , int );
double multiplier(double , double);
void afficher(int);*/

int main(int argc, char *argv[])
{
    /* Appel de la fonction */
    double res = multiplier(3.4, 5.2);
    printf("%f\n", res);
    printf("%f\n", multiplier(2.0, 3.0));

    /* Appel de methode (sans retour) */
    afficher(234);

    varGlobal = 34;
    varGlobalStatic = 100;

    // Atelier: Fonction maximum
    int a, b;
    scanf("%d %d", &a, &b);
    printf("Le maximum est %d", maximum(a, b));

    // Atelier: Fonction paire
    printf("%d\n", even(3));
    printf("%d\n", even(4));

    /* Paramètre passé par valeur (par défaut)
      C'est une copie de la valeur du paramètre qui est transmise à la fonction */
    int a = 42;
    testParamValeur(a);
    printf("3 v=%d", a); /*42*/

    /* Paramètre passé par adresse
       La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la fonction */
    printf("%p", &a);
    testParamAdresse(&a);
    printf("3 v=%d\n", a); /*123*/

    int inc = 0;
    incrementer(&inc);
    printf("inc=%d\n", inc);

    /* Atelier permuter */
    int perA = 2, perB = 10;
    printf("perA=%d perB=%d\n", perA, perB);
    permuter(&perA, &perB);
    printf("perA=%d perB=%d\n", perA, perB);

    /* Les tableaux peuvent être passé par adresse et pas par valeur */
    int t[] = {4, 7, 2, 9, 1, 45};
    afficherTab(t, 6);

    /* Fonction récurssive */
    int f = factorial(3);
    printf("3!=%d\n", f);

    /* Paramètre de la fonction main */
    int i;
    printf("argc=%d\n", argc);
    for (i = 0; i < argc; i++)
    {
        printf("%s\n", argv[i]);
    }

    /* Nombre de paramètre variable */
    printf("%f\n", moyenne(3, 1, 3, 2));
    printf("%f\n", moyenne(1, 21));
    printf("%f\n", moyenne(5, 21, 4, 1, 78, 1));
    printf("%f\n", moyenne(0));

    /* Variable locale */
    int lo = 23;
    if (lo > 0)
    {
        int loi = 5;
        lo = loi + 3;
        printf("loi=%d", loi);
    }
    printf("lo=%d", lo);

    /* Variable globale*/
    printf("variable globale=%d", varGlobal); /* 34 */

    /* Masquage de variable */
    int varGlobal = 3; /* La variable locale va masquer la variable globale*/
    printf("varGlobal=%d", varGlobal);

    /*Classe de mémorisation
     Variable locale
     Classe de mémorisation static */
    testStatic();
    testStatic();
    testStatic();
    testStatic();

    /* Variable globale
    Classe de mémorisation  register */
    testRegister();

    /* Classe de mémorisation  extern */
    testExtern();

    printf("%d\n", varGlobalStatic);
    testGlobalStatic();
    printf("%d\n", varGlobalStatic);

    testExternLocal();

    printf("%f", INIT_COMPTEUR);

    /* Atelier: bissextile */
    printf("annee 2023=%d\n", bissextile(2023));
    printf("annee 1900=%d\n", bissextile(1900));
    printf("annee 2024=%d\n", bissextile(2024));
    printf("annee 2000=%d\n", bissextile(2000));

    /* Atelier: dernier jour du mois */
    printf("%d/02/2000\n", dernierJourMois(2000, 2));
    printf("%d/02/2023\n", dernierJourMois(2023, 2));
    printf("%d/07/2023\n", dernierJourMois(2023, 7));
    printf("%d/11/2023\n", dernierJourMois(2023, 11));

    return 0; /* -> indique au système d'explotation que la fonction c'est bien déroulée*/
}

/* Définition des fonctions -> déplacer dans un fichier sources .c */
/*int even(int val)
{
    return !(val & 1);
}

int maximum(int v1, int v2)
{
    return v1 > v2 ? v1 : v2;
}

double multiplier(double d1, double d2)
{
    return d1 * d2;
}

void afficher(int val)
{
    printf("%d", val);
}
*/
