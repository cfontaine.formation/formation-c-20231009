#include "classeMemorisation.h"
#include <stdio.h>

/*extern int varGlobal;*/
/*extern int varGlobalStatic;*/
extern double PI;

void testRegister(void){
    register int i; /* indique au complateur de stocker la variable dans un registre*/
    int res=0;
    for(i=0;i<1000;i++){
        res+=i;
    }
}

/* Classe de mémorisation static */
void testStatic(void){
    /* Entre 2 exécutions vStat conserve sa valeur */
    static int vStat; /* par défaut initialisé à 0 */
    printf("vStat=%d\n",vStat);
    vStat++;    /* vStat n'est visible que dans la fonction */
 }

void testExtern(void){
/*  printf("%d",varGlobal);*/  
    printf("%f",PI);
}


void testExternLocal(void){
    extern int varGlobal;
    printf("%d",varGlobal);
}

void testGlobalStatic(void)
{
  /*  varGlobalStatic=1200; */
}
