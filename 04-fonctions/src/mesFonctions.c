#include <stdio.h>
#include <stdarg.h>
#include "mesFonctions.h"

/* Fichier .c  -> contient les Définitions des fonctions */

double multiplier(double d1, double d2)
{
    return d1 * d2;
}

void afficher(int val)
{
    printf("%d", val);
    /* return ;*/
}

/* Atelier fonction maximum :
   Écrire une fonction maximum qui prends en paramètre 2 nombres entierset elle retourne le maximum
   Saisir 2 nombres et afficher le maximum entre ces 2 nombres*/
int maximum(int v1, int v2)
{
    return v1 > v2 ? v1 : v2;

    /*  if(v1>v2)
      {
          return v1;
      }
      else
      {
          return v2;
      }*/
}

/* Atelier fonction Paire
   Écrire une fonction even qui prend un entier en paramètre
   Elle retourne 1 si il est paire */
int even(int val)
{
    return !(val & 1); /* ou return val%2==0;*/

    /* if((val%2)==0)
     {
         return 1;
     }
     else
     {
         return 0;
     }*/
}

/* Passage de paramètres par valeur
   La valeur du paramètre est copiée et une modification sur la copie n’entraîne pas la modification de l’original */
void testParamValeur(int v)
{
    printf("1 v=%d\n", v);
    v = 123;
    printf("2 v=%d\n", v);
}

/* Passage de paramètres par adresse
   En utilisant le passage de paramètres par adresse, on modifie réellement la variable qui est passée en paramètre*/
void testParamAdresse(int *ptr)
{
    printf("ptr=%p\n", ptr);
    printf("1 v=%d\n", *ptr);
    *ptr = 123;
    printf("2 v=%d\n", *ptr);
}

void incrementer(int *val)
{
    (*val)++;
}

/* Permuter
   Écrire une fonction swap qui va permettre de permuter les valeurs des 2 variables entières  passées en paramètres */
void permuter(int *ptr1, int *ptr2)
{
    int tmp = *ptr1;
    *ptr1 = *ptr2;
    *ptr2 = tmp;
}

/* Passage d'un tableau, comme paramètre d'une fonction
  On ne peut pas passer un tableau par valeur uniquement par adresse */
void afficherTab(int t[], int size)
{
    int i;
    printf("[ ");
    for (i = 0; i < size; i++)
    {
        printf("%d ", t[i]);
    }
    printf("]\n");
}

/* Fonction recursive */
int factorial(int n) /* factoriel= 1* 2* … n */
{
    if (n <= 1) /* condition de sortie */
    {
        return 1;
    }
    else
    {
        return factorial(n - 1) * n;
    }
}

/* Nombre d'arguments variable */
double moyenne(int nbParam, ...)
{
    va_list val;
    va_start(val, nbParam); /* initialise la liste des arguments variables. On passe en paramètre le nom du dernier paramètre nommé */

    double somme = 0.0;
    int i;
    if (nbParam == 0)
    {
        return 0.0;
    }
    for (i = 0; i < nbParam; i++)
    {
        somme += va_arg(val, int); /* va_arg permet de recupérer les valeurs des arguments dans le même ordre que celui transmis à la fonction
       }							   On passe en paramètre le type de l’argument */
    }
    va_end(val); /* nettoyage des arguments */
    return somme / nbParam;
}

/* Atelier: Années bissextiles
   Écrire une fonction bissextile qui indique si Une année est bissextile (retourne 1) sinon elle retourne 0 Une année est bissextile
   - si elle est divisible par 4, à l'exception des années divisibles par 100
   - si elle est divisible par 400 */
int bissextile(int annee)
{
    return (annee % 4 == 0 && annee % 100 != 0) | annee % 400 == 0;
}

/* Atelier: Dernier jour du mois
   Écrire un fonction qui retourne le dernier jour du mois (30,31,29 ou 28) en fonction de l'année et du mois.*/
int dernierJourMois(int annee, int mois)
{
    switch (mois)
    {
    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
    case 2:
        return bissextile(annee) ? 29 : 28;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
    default:
        return -1;
    }
}