#include "fichier.h"

int main(void)
{
    /* Fichie texte */
    /* Écriture/ Lecture non formatée  (fputs/fgets) */
    ecrireTexte("../test.txt");
    lireTexte("../test.txt");

    /* Écriture/ Lecture formatée (fprintf/fscanf) */
    ecritureTexteFormat("../test2.txt");
    lectureTexteFormat("../test2.txt");
   
    /* Fichie binaire */
    struct compteBancaire cs[] = {{100.0, "fr5962-4", "John Doe"}, {500.0, "fr5962-6", "Alan Smithee"}, {200.0, "fr5962-5", "Jane Doe"}};
    /* Écriture de fichier binaire (fwrite) */
    ecritureBin("../cb.bin", cs, 3);

    /* Lecture de fichier binaire (fread) */
    struct compteBancaire cb[3];
    lectureBin("../cb.bin", cb, 3);
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        afficherCompte(&cb[i]);
    }

    /* Déplacement du curseur dans le fichier */
    testDeplacement("../cb.bin");
    return 0;
}