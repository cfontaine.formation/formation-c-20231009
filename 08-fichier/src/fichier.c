#include "fichier.h"
#include <stdio.h>

void ecrireTexte(char *path)
{
    FILE *f = fopen(path, "wt");
    if (f != NULL)
    {
        int i;
        for (i = 0; i < 10; i++)
        {
            fputs("Hello World\n", f);
        }
        fclose(f);
    }
}

void lireTexte(char *path)
{
    FILE *f = fopen(path, "rt");
    if (f != NULL)
    {
        char str[128];
        while (!feof(f))
        {
            if (fgets(str, 128, f) != NULL)
            {
                printf("%s", str);
            }
        }
        fclose(f);
    }
}

void ecritureTexteFormat(char *path)
{
    FILE *f = fopen(path, "wt");
    if (f != NULL)
    {
        int i = 0;
        for (i = 0; i < 10; i++)
        {
            fprintf(f, "%d %s\n", i, "Hello World!");
        }
        fclose(f);
    }
}

void lectureTexteFormat(char *path)
{
    FILE *f = fopen(path, "rt");
    if (f != NULL)
    {
        int nbLigne;
        char str1[128];
        char str2[128];
        while (!feof(f))
        {
            if (fscanf(f, "%d %s %s\n", &nbLigne, str1, str2) > 0)
            {
                printf("%d %s %s\n", nbLigne, str1, str2);
            }
        }
        fclose(f);
    }
}

void ecritureBin(char *path, tCompteBancaire cb[], int size)
{
    FILE *f = fopen(path, "wb");
    if (f != NULL)
    {
        fwrite(cb, sizeof(tCompteBancaire), size, f);
        fclose(f);
    }
}

void lectureBin(char *path, tCompteBancaire cb[], int size)
{
    FILE *f = fopen(path, "rb");
    if (f != NULL)
    {
        fread(cb, sizeof(tCompteBancaire), size, f);
        fclose(f);
    }
}


void testDeplacement(char *path)
{
    struct compteBancaire cb;
    FILE *f = fopen(path, "r+b");
    if (f != NULL)
    {
        /* On déplace le curseur de 2 fois la taille d'un compte bancaire
           à partir du début du fichier */
        fseek(f, 2*sizeof(struct compteBancaire), SEEK_SET);
        fread(&cb, sizeof(struct compteBancaire), 1, f);
        afficher(&cb);
        fclose(f);
    }
}
