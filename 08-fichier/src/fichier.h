#include "compteBancaire.h"
#ifndef FICHIER_H
#define FICHIER_H

void ecrireTexte(char *);
void lireTexte(char *);
void ecritureTexteFormat(char *path);
void lectureTexteFormat(char *path);
void ecritureBin(char *path, tCompteBancaire cb[], int size);
void lectureBin(char *path, tCompteBancaire cb[], int size);

#endif