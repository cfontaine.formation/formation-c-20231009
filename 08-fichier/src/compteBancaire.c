#include "compteBancaire.h"
#include <stdio.h>

void afficherCompte(tCompteBancaire *cb)
{
    printf("\nSolde=%f\nIban=%s\nTitulaire=%s\n",cb->solde,cb->iban,cb->titulaire);
}
void crediterCompte(tCompteBancaire *cb, double valeur)
{
    if(valeur>0)
    {
        cb->solde+=valeur;
    }
}
void debiterCompte(tCompteBancaire *cb, double valeur)
{
    if(valeur>0)
    {
        cb->solde-=valeur;
    }
}
int estPositif(tCompteBancaire *cb)
{
    return cb->solde>=0;
}