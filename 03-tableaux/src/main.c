#include <stdio.h>
/* #include <limits.h> */

int main(void)
{
    /* Tableau à une dimmension

    Déclaration du tableau */
    double t[5];

    /* Initialisation des éléments du tableau */
    /* t[0]=0.0;  /* Accès à un élément du tableau */
    /* t[1]=0.0;
     t[2]=0.0;
     t[3]=0.0;
     t[4]=0.0;*/
    int i;
    for (i = 0; i < 5; i++)
    {
        t[i] = 0.0;
    }

    /* Accès à un élément*/
    t[1] = 23.4;
    printf("%f\n", t[0]);

    /* Parcourir un tableau à une dimmension */
    for (i = 0; i < 5; i++)
    {
        printf("t[%d]=%.10f\n", i, t[i]);
    }

    /* erreur : accès en dehors du tableau 
    /!\ Il n'y pas de verification avec le C */
    /* printf("%f",t[40]);*/

    /* Calculer le nombre d'élément d'un tableau*/
    int nbElement = sizeof(t) / sizeof(t[0]);
    printf("%d %d %d\n", sizeof(t), sizeof(t[0]), nbElement);

    /* Déclaration et initialisation d'un tableau */
    int tInt[] = {4, 7, 1, 6}; /* Taille du tableau le nombre de valeur*/
    for (i = 0; i < 4; i++)
    {
        printf("%d\n", tInt[i]);
    }

    int tInt2[5] = {4, 5, 3, 6, 5};
    for (i = 0; i < 5; i++)
    {
        printf("%d\n", tInt2[i]);
    }

    int tInt3[5] = {4, 5}; // Les 3 valeurs qui ne sont pas définie sont égale à 0
                           // 4,5,0,0,0
    for (i = 0; i < 5; i++)
    {
        printf("%d\n", tInt3[i]);
    }

    int tInt4[6] = {0}; // Tous les éléments du tableau sont initialisés à 0
    for (i = 0; i < 6; i++)
    {
        printf("%d\n", tInt4[i]);
    }

    /* Atelier : tableau
       Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3 */
    int tab[] = {-7, -4, -8, -10, -3};
    int max = tab[0]; /*INT_MIN;*/
    double somme = 0.0;
    for (i = 0; i < 5; i++)
    {
        if (tab[i] > max)
        {
            max = tab[i];
        }
        somme += tab[i]; /*somme= somme + tab[i]*/
    }
    double moyenne = somme / 5;
    printf("maximum=%d Moyenne=%f", max, moyenne);

    /* Tableau à 2 dimmensions */
    /* Déclaration */
    int tab2d[3][4];

    /* Initialisation */
    int j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            tab2d[i][j] = 0;
        }
    }

    /* Accès à un élément */
    tab2d[1][0] = 42;
    printf("%d\n", tab2d[1][0]);

    /* Parcourrir un tableau à 2 dimensions */
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            printf("%d\t", tab2d[i][j]);
        }
        printf("\n");
    }
    /* Pour les tableaux à n dimension ont peut entourer les lignes avec des accolades*/
    /*double t2d[4][3]={{4.5,6.0,3.4},{6.5,-4.0,1.4},{4.6,-6.0,0.4},{11.5,7.0,5.4}};
    double t2d[4][3]={4.5,6.0,3.4,6.5,-4.0,1.4,4.6,-6.0,0.4,11.5,7.0,5.4};*/
    /* les valeurs d'initialisations qui ne sont pas définie sont initialisé à 0  */
    double t2d[4][3] = {4.5, 6.0, 3.4, 6.5, -4.0};
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            printf("%f\t", t2d[i][j]);
        }
        printf("\n");
    }

    /* Déclaration d'un tableau à 3 dimmensions*/
    int tab3d[3][2][4];

    return 0;
}