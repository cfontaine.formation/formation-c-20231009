#include <stdio.h>
#include "point.h"
#include "cercle.h"
#include "compteBancaire.h"
#include "liste.h"
#include "mesStructures.h"

    struct point3d
    {
        double x ;
        double y;
        double z;
    } p3d;

enum motorisation {ESSENCE=95,DIESEL=3,GPL=35,ELECTRIQUE=2,ETHANOL=45};

int addition(int x,int y)
{
    return x+y;
}

int main(void){


    /* Déclaration et initialisation*/
    struct point p1,p2;
    p1.x=0;
    p1.y=0;

    p2.x=1;
    p2.y=2;

    struct point p3={1,1};

    printf("x=%d y=%d\n",p1.x,p1.y);




    struct point p4;
    /* Affectation */
    p4=p2;
    printf("p4(%d %d) p2(%d %d)\n",p4.x,p4.y,p2.x,p2.y);

    /* Compararaison de structure*/
    if(p4.x==p2.x && p4.y==p2.y)
    {
        printf("Les 2 points sont egaux\n");
    }

    /* Tableau de structure */
    struct point tab[3];
    tab[0].x=3;
    tab[0].y=2;
    tab[1].x=-1;
    tab[1].y=2;
    tab[2].x=7;
    tab[2].y=-4;
    int i;
    for(i=0;i<3;i++){
        printf("(%d,%d)\n",tab[i].x,tab[i].y);
    }

    struct point tab2[]={{1,4},{5,6}};

    /* structure et fonction */
    struct point a,b;
    initialisationPoint(&a,2,3);
    initialisationPoint(&b,-2,8);

    afficherPoint(&a);
    afficherPoint(&b);

    printf("%f",distance(&a,&b));

    /* Structure imbiquée*/
    struct cercle c1;
    c1.rayon=2.0;
    c1.centre.x=2;
    c1.centre.y=2;

    afficherCercle(&c1);


    /* typedef*/
     tpoint r={3,6};

    typedef int (*operation)(int,int);

    operation op=addition;
    printf("%d",op(1,3));


    tCompteBancaire cb1={350.0,"fr5962-01","John Doe"};
    afficherCompte(&cb1);
    crediterCompte(&cb1,100.0);
    afficherCompte(&cb1);
    debiterCompte(&cb1,235.0);
    afficherCompte(&cb1);
    printf("%d\n",estPositif(&cb1));

    /* Liste simplement chainée*/
    ptrElementList tl=initList();
    ajouterElement(&tl,6);
    ajouterElement(&tl,-2);
    ajouterElement(&tl,1);
    ajouterElement(&tl,5);
    ajouterElement(&tl,4);
    afficherList(tl);
    effacerList(&tl);

    /* Union */
    union prime pri;
    pri.fixe=1000;
    printf("%d\n",pri.fixe);
    pri.taux=0.10;
    printf("%f\n",pri.taux);
    printf("%f\n",pri.fixe);

    /* Champs de bits*/
    struct mot m;
    m.bp1=0;
    m.leds=0x1F;
    m.bp2a6=0x3;

    printf("%x %x %x\n",m.bp1,m.leds,m.bp2a6);
    /* Enumération */

    enum motorisation mt=GPL;
    printf("%d\n",mt);
    /* enum -> int */
    int imt=mt;
    printf("%d\n",imt);
    return 0;
}