#ifndef COMPTE_BANCAIRE_H
#define COMPTE_BANCAIRE_H

struct compteBancaire
{
    double solde;
    char iban[10];
    char titulaire[30];
};

typedef struct compteBancaire tCompteBancaire;

void afficherCompte(tCompteBancaire *);
void crediterCompte(tCompteBancaire *, double );
void debiterCompte(tCompteBancaire *, double );
int  estPositif(tCompteBancaire *);

#endif