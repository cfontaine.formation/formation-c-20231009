#include "point.h"
#include <stdio.h>
#include <math.h>

void afficherPoint(struct point* p)
{
    printf("(%d,%d)",p->x,p->y); /* *(p).x*/
}

void initialisationPoint(struct point*p , int xi ,int yi)
{
    p->x=xi;
    p->y=yi;
}

double distance(struct point *a, struct point *b)
{
    return sqrt(pow(b->x-a->x,2)+pow(b->y-a->y,2));
}