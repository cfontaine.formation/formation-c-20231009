#include "point.h"

#ifndef CERCLE_H
#define CERCLE_H

/* structure imbriqué*/
struct cercle
{
    double rayon;
    struct point centre;
};

void afficherCercle(struct cercle* c);
#endif // !CERCLE_H