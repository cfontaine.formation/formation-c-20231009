#ifndef POINT_H
#define POINT_H

struct point
{
    int x;
    int y;
};

typedef struct point tpoint;

void afficherPoint(struct point* );
void initialisationPoint(struct point*, int,int);
double distance(struct point *a, struct point *b);

#endif 