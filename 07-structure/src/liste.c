#include "liste.h"
#include <stdio.h>
#include <stdlib.h>

ptrElementList initList()
{
    return NULL;
}

void ajouterElement(ptrElementList* list,int data)
{
    ptrElementList elm=malloc(sizeof(struct elementList));
    elm->data=data;
    elm->suivant=*list;
    *list=elm;
}

void afficherList(ptrElementList list)
{
    ptrElementList ptrElem=list;
    while(ptrElem!=NULL)
    {
        printf("%d\n",ptrElem->data);
        ptrElem=ptrElem->suivant;
    }
}

void effacerList(ptrElementList * list)
{
    ptrElementList ptrElem=*list;
    ptrElementList ptrDel;
     while(ptrElem!=NULL)
    {
        ptrDel=ptrElem;
        ptrElem=ptrElem->suivant;
        free(ptrDel);
    }
    *list=NULL;
}